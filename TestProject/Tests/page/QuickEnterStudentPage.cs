﻿using OpenQA.Selenium;
using TestProject.Tests.helpers.constantsXpath;

namespace testTask.pages
{
    class QuickEnterStudentPage
    {
        private IWebDriver driver;
        private IWebElement fieldSurnameInput;
        private IWebElement fieldNameInput;
        private IWebElement fieldPatronymicInput;
        private IWebElement fieldDatуOfBirthInput;
        private IWebElement fieldLoginInput;
        private IWebElement fieldPasswordInput;
        private IWebElement fieldСonfirmationPasswordInput;
        private IWebElement buttonSave;
        private IWebElement buttonOk;


        public IWebElement getFieldSurnameInput() { return fieldSurnameInput ?? (fieldSurnameInput = driver.FindElement(QuickEnterStudentXpath.fieldSurnameInputXpath)); }

        public IWebElement getFieldNameInput() { return fieldNameInput ?? (fieldNameInput = driver.FindElement(QuickEnterStudentXpath.fieldNameInputXpath)); }

        public IWebElement getFieldPatronymicInput() { return fieldPatronymicInput ?? (fieldPatronymicInput = driver.FindElement(QuickEnterStudentXpath.fieldPatronymicInputXpath)); }

        public IWebElement getFieldDatуOfBirthInput() { return fieldDatуOfBirthInput ?? (fieldDatуOfBirthInput = driver.FindElement(QuickEnterStudentXpath.fieldDatуOfBirthInputXpath)); }

        public IWebElement getFieldLoginInput() { return fieldLoginInput ?? (fieldLoginInput = driver.FindElement(QuickEnterStudentXpath.fieldLoginInputXpath)); }

        public IWebElement getFieldPasswordInput() { return fieldPasswordInput ?? (fieldPasswordInput = driver.FindElement(QuickEnterStudentXpath.fieldPasswordInputXpath)); }

        public IWebElement getFieldСonfirmationPasswordInput() { return fieldСonfirmationPasswordInput ?? (fieldСonfirmationPasswordInput = driver.FindElement(QuickEnterStudentXpath.fieldСonfirmationPasswordInputXpath)); }

        public IWebElement getButtonSave() { return buttonSave ?? (buttonSave = driver.FindElement(QuickEnterStudentXpath.buttonSaveXpath)); }

        public IWebElement getButtonOk() { return buttonOk ?? (buttonOk = driver.FindElement(QuickEnterStudentXpath.buttonOkEventPopUpXpath)); }

        public QuickEnterStudentPage(IWebDriver driver)
        {
            this.driver = driver;
        }
    }
}
