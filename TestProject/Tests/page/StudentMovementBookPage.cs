﻿using OpenQA.Selenium;
using TestProject.Tests.helpers.constantsXpath;

namespace testTask.pages
{
    class StudentMovementBookPage
    {
        private IWebDriver driver;
        private IWebElement captionStudentMovementBook;
        private IWebElement selectTypeDocument;
        private IWebElement selectImportStudentsTypeDocument;
        private IWebElement buttonAddStudents;
        private IWebElement warningEventPopUp;
        private IWebElement buttonOkWarningEventPopUp;
        private IWebElement buttonSave;

        public IWebElement getСaptionStudentMovementBook() { return captionStudentMovementBook ?? (captionStudentMovementBook = driver.FindElement(StudentMovementBookXpath.fieldEventTypeEmptyXpath)); }

        public IWebElement getSelectTypeDocument() { return selectTypeDocument ?? (selectTypeDocument = driver.FindElement(StudentMovementBookXpath.selectTypeDocumentXpath)); }

        public IWebElement getselectImportStudentsTypeDocument() { return selectImportStudentsTypeDocument ?? (selectImportStudentsTypeDocument = driver.FindElement(StudentMovementBookXpath.selectImportStudentsTypeDocumentXpath)); }

        public IWebElement getButtonAddStudents() { return buttonAddStudents ?? (buttonAddStudents = driver.FindElement(StudentMovementBookXpath.buttonAddStudentsXpath)); }

        public IWebElement getWarningEventPopUp() { return warningEventPopUp ?? (warningEventPopUp = driver.FindElement(StudentMovementBookXpath.labelWarningEventPopUpXpath)); }

        public IWebElement getButtonOkWarningEventPopUp() { return buttonOkWarningEventPopUp ?? (buttonOkWarningEventPopUp = driver.FindElement(StudentMovementBookXpath.buttonOkWarningEventPopUpXpath)); }

        public IWebElement getButtonSave() { return buttonSave ?? (buttonSave = driver.FindElement(StudentMovementBookXpath.buttonSaveXpath)); }

        public StudentMovementBookPage(IWebDriver driver)
        {
            this.driver = driver;
        }
    }
}
