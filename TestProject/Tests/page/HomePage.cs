﻿using OpenQA.Selenium;
using TestProject.Tests.helpers.constantsXpath;

namespace testTask.pages
{
    class HomePage
    {
        private IWebDriver driver;
        private IWebElement elemenToptMenuNameControl;
        private IWebElement inportStudentButtonInMenu;

        public IWebElement getElemenToptMenuNameControl() { return elemenToptMenuNameControl ?? (elemenToptMenuNameControl = driver.FindElement(HomePageConstantsXpath.elemenToptMenuNameControlXpath)); }

        public IWebElement getInportStudentButtonInMenu() {
            return inportStudentButtonInMenu ?? (inportStudentButtonInMenu = driver.FindElement(HomePageConstantsXpath.inportStudentButtonInMenuXpath));
        }

        public HomePage(IWebDriver driver)
        {
            this.driver = driver;
        }
    }
}
