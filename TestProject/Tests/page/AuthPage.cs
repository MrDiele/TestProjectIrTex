﻿using OpenQA.Selenium;
using System;
using TestProject.Tests.helpers.constantsXpath;

namespace testTask.pages
{
    public class AuthPage
    {
        private IWebElement loginField;
        private IWebElement passwordField;
        private IWebElement buttonSignIn;
        private IWebDriver driver;
        private IWebElement generalEducationalTypeSchoolField;
        private IWebElement school108Field;

        public IWebElement getGeneralEducationalTypeSchoolField() { return generalEducationalTypeSchoolField ?? (generalEducationalTypeSchoolField = driver.FindElement(AuthPageConstantsXpath.generalEducationalTypeSchoolFieldXpath)); }

        public IWebElement getSchool108Field() { return school108Field ?? (school108Field = driver.FindElement(AuthPageConstantsXpath.school108FieldXpath)); }

        public AuthPage(IWebDriver driver, By locForLogin, 
            By locForPassword, By locForButton)
        {
            this.driver = driver;
            setLoginField(driver, locForLogin);
            setPasswordField(driver, locForPassword);
            setButtonSignIn(driver, locForButton);
        }

        public AuthPage(IWebDriver driver)
        {
            this.driver = driver;
        }

        public void authorization(String login, String password)
        {
            getGeneralEducationalTypeSchoolField().Click();
            getSchool108Field().Click();            
            entryLogin(login);
            entryPassword(password);
            clickButtonSignIn();
        }

        private void entryLogin(String login)
        {
            loginField.SendKeys(login);
        }

        private void entryPassword(String password)
        {
            passwordField.SendKeys(password);
        }

        private void clickButtonSignIn()
        {
            buttonSignIn.Click();
        }

        private void setLoginField(IWebDriver driver, By locator)
        {
            loginField = driver.FindElement(locator);
        }

        private void setPasswordField(IWebDriver driver, By locator)
        {
            passwordField = driver.FindElement(locator);
        }

        private void setButtonSignIn(IWebDriver driver, By locator)
        {
            buttonSignIn = driver.FindElement(locator);
        }
        
        public IWebElement getLoginField() { return loginField; }
        public IWebElement getPasswordField() { return passwordField; }
        public IWebElement getButtonSignIn() { return buttonSignIn; }
    }
}
