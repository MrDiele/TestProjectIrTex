﻿using OpenQA.Selenium;
using TestProject.Tests.helpers.constantsXpath;

namespace testTask.pages
{
    class ImportStudentPage
    {
        private IWebDriver driver;
        private IWebElement numberDocument;
        private IWebElement dateDocument;
        private IWebElement addStudentInDocument;
        private IWebElement eventPopUpAddButton;

        public IWebElement getNumberDocument() { return numberDocument ?? (numberDocument = driver.FindElement(ImportStudentXpath.numberDocumentXpath)); }

        public IWebElement getDateDocument() { return dateDocument ?? (dateDocument = driver.FindElement(ImportStudentXpath.dateDocumentXpath)); }

        public IWebElement getAddStudentInDocument() { return addStudentInDocument ?? (addStudentInDocument = driver.FindElement(ImportStudentXpath.addStudentInDocumentXpath)); }

        public IWebElement getEventPopUpAddButton() { return eventPopUpAddButton ?? (eventPopUpAddButton = driver.FindElement(ImportStudentXpath.eventPopUpAddButtonXpath)); }

        public ImportStudentPage(IWebDriver driver)
        {
            this.driver = driver;
        }

    }
}