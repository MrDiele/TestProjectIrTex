﻿using OpenQA.Selenium;

namespace TestProject.Tests.helpers.constantsXpath
{
    public class AuthPageConstantsXpath
    {
        public static By generalEducationalTypeSchoolFieldXpath = By.XPath("//*[@id=\"funcs\"]/option[3]");
        public static By school108FieldXpath = By.XPath("//*[@id=\"schools\"]/option[2]");

        public static By loginFieldXpath = By.XPath("//*[@id=\"message\"]//div[8]/input");
        public static By passwordFieldXpath = By.XPath("//*[@id=\"message\"]//div[9]/input");
        public static By buttonloginXpath = By.XPath("//*[@id=\"message\"]//div[12]/a/span");
    }
}
