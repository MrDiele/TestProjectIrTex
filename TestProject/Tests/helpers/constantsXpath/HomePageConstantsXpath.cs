﻿using OpenQA.Selenium;

namespace TestProject.Tests.helpers.constantsXpath
{
    class HomePageConstantsXpath
    {
        public static By elemenToptMenuNameControlXpath = By.XPath("//li[@class='dropdown'][1]/a");
        public static By inportStudentButtonInMenuXpath = By.XPath("//li[14]/a");
    }
}
