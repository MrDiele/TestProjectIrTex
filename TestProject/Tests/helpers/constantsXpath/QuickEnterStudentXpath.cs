﻿using OpenQA.Selenium;

namespace TestProject.Tests.helpers.constantsXpath
{
    class QuickEnterStudentXpath
    {
        public static By fieldSurnameInputXpath = By.XPath("//div[3]/div//input");
        public static By fieldNameInputXpath = By.XPath("//div[4]//input");
        public static By fieldPatronymicInputXpath = By.XPath("//div[5]//input");
        public static By fieldDatуOfBirthInputXpath = By.XPath("//div[6]//input");
        public static By fieldLoginInputXpath = By.XPath("//div[8]//input");
        public static By fieldPasswordInputXpath = By.XPath("//div[9]//input");
        public static By fieldСonfirmationPasswordInputXpath = By.XPath("//div[10]//input");
        public static By buttonSaveXpath = By.XPath("//button[@class='btn btn-primary']");
        public static By buttonOkEventPopUpXpath = By.XPath("//button[@id='39dcaa08-390f-4773-b413-db687e67d5ff']");
    }
}
