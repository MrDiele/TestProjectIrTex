﻿using OpenQA.Selenium;

namespace TestProject.Tests.helpers.constantsXpath
{
    class ImportStudentXpath
    {
        public static By numberDocumentXpath = By.XPath("//div[4]//input[@class='form-control']");
        public static By dateDocumentXpath = By.XPath("//input[@class='form-control date-input']");
        public static By addStudentInDocumentXpath = By.XPath("//button[@class='btn btn-info']");
        public static By eventPopUpAddButtonXpath = By.XPath("//*[@id=\"f5953f1b - afac - 4899 - b5ea - 6f34332d8c83\"]");
    }
}
