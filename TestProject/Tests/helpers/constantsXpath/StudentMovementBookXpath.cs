﻿using OpenQA.Selenium;

namespace TestProject.Tests.helpers.constantsXpath
{
    class StudentMovementBookXpath
    {
        public static By fieldEventTypeEmptyXpath = By.XPath("//div[2]/h1[contains(text(),\'Книга движения учащихся\')]");
        public static By selectTypeDocumentXpath = By.XPath("//div[2]/div/select");
        public static By selectImportStudentsTypeDocumentXpath = By.XPath("//div[2]/div/select/option[6]");
        public static By buttonAddStudentsXpath = By.XPath("//div[1]/div/div/div[1]/button");
        public static By labelWarningEventPopUpXpath = By.XPath("//*[@id=\"bdf3cdae-6161-4d44-9ddb-686ab03baa5f\"]//text()[2]");
        public static By buttonOkWarningEventPopUpXpath = By.XPath("//*[@id=\"40d213e0-4e2a-4522-bd04-9751edc780be\"]");
        public static By buttonSaveXpath = By.XPath("//div[1]/button[1]");
    }
}
