﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using TestProject.Tests.helpers.constantsXpath;
using testTask.pages;


namespace TestProject.Tests
{
    [TestFixture]
    public class CheckEventAT
    {
        private IWebDriver driver;
        private Uri hubUrl;
        private const String basicUrl = "https://sgo2test.ir-tech.ru/";
        private const String silemiumGrid = "http://localhost:4444/wd/hub";
        private const String login = "admin";
        private const String password = "admin108";
        private AuthPage pageAuth;
        private HomePage pageHome;
        private ImportStudentPage pageImportStudent;
        private StudentMovementBookPage pageStudentMovementBook;
        private QuickEnterStudentPage pageQuickEnterStudent;
        
        [TestFixtureSetUp]
        public void BeforeTestClass()
        {
            //вставить вод для проверки наличия висящего процесса "chromedriver" и уничтожения его. Запуска батника для seleniumGrid
        }

        [SetUp]
        public void SetUp() // установить задержки
        {
            hubUrl = new Uri(silemiumGrid);
            DesiredCapabilities capability = new DesiredCapabilities();
            capability.SetCapability("browserName", "chrome");
            driver = new RemoteWebDriver(hubUrl, capability);
            driver.Navigate().GoToUrl(basicUrl);
            driver.Manage().Window.Maximize();
#pragma warning disable CS0618 // Type or member is obsolete
            driver.Manage().Timeouts().SetPageLoadTimeout(TimeSpan.FromSeconds(25));
#pragma warning disable CS0618 // Type or member is obsolete
            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(25));
            pageAuth = new AuthPage(driver, AuthPageConstantsXpath.loginFieldXpath,
            AuthPageConstantsXpath.passwordFieldXpath, AuthPageConstantsXpath.buttonloginXpath);
            pageAuth.authorization(login, password);
            pageHome = new HomePage(driver);
            Assert.IsTrue(pageHome.getElemenToptMenuNameControl().Displayed);
        }

        [Test]
        public void ImportChildrenTest()
        {
            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(25));
            pageImportStudent = new ImportStudentPage(driver);
            pageQuickEnterStudent = new QuickEnterStudentPage(driver);
            pageStudentMovementBook = new StudentMovementBookPage(driver);
            pageHome.getElemenToptMenuNameControl().Click();
            pageHome.getInportStudentButtonInMenu().Click();
            Assert.IsTrue(pageStudentMovementBook.getСaptionStudentMovementBook().Displayed);
            pageStudentMovementBook.getSelectTypeDocument().Click();
            pageStudentMovementBook.getselectImportStudentsTypeDocument().Click();
            pageStudentMovementBook.getButtonAddStudents().Click();
            pageImportStudent.getNumberDocument().SendKeys("1");
            pageImportStudent.getDateDocument().SendKeys("5.02.15");
            pageImportStudent.getAddStudentInDocument().Click();
            pageImportStudent.getEventPopUpAddButton().Click();
            pageQuickEnterStudent.getFieldSurnameInput().SendKeys("Иванов");
            pageQuickEnterStudent.getFieldNameInput().SendKeys("Иван");
            pageQuickEnterStudent.getFieldPatronymicInput().SendKeys("Иванович");
            pageQuickEnterStudent.getFieldDatуOfBirthInput().SendKeys("11.02.09");
            pageQuickEnterStudent.getFieldLoginInput().SendKeys("Ivanov09");
            pageQuickEnterStudent.getFieldPasswordInput().SendKeys("ivanov110209");
            pageQuickEnterStudent.getFieldСonfirmationPasswordInput().SendKeys("ivanov110209");
            pageQuickEnterStudent.getButtonSave().Click();
            pageQuickEnterStudent.getButtonOk().Click();
            Assert.IsTrue(pageStudentMovementBook.getWarningEventPopUp().Text.Contains("Документ о движении успешно создан"));
            pageStudentMovementBook.getButtonOkWarningEventPopUp().Click();
            pageStudentMovementBook.getButtonSave().Click();
            Assert.IsTrue(pageStudentMovementBook.getСaptionStudentMovementBook().Displayed);
        }

        [TearDown]
        public void AfterTest()
        {
            driver.Quit();
        }

        [TestFixtureTearDown]
        public void AfterTestClass()
        {
            //вставить код для проверки наличия висящего процесса "chromedriver" и уничтожения его, закрытие двух cmd(hub, node) seleniumGrid
        }
    }
}
